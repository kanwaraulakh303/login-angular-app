import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './app-footer.component.html',
  styleUrls: ['./app-footer.component.css']
})
export class AppFooterComponent implements OnInit {
  copyinfoyear:number = new Date().getFullYear();
  copyInfo:string;
  constructor() {
     this.copyInfo = "© "+this.copyinfoyear;
   }

  ngOnInit() {}

}
