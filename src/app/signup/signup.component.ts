import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { trigger, transition, useAnimation,state,animate,style } from '@angular/animations';
import { tada ,flipOutY,flipOutX} from 'ng-animate';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  animations: [

    trigger('error', [
      
      transition('* => error', useAnimation(tada)),
      transition('* => success', useAnimation(flipOutX)),
    ]),
  
  ]
})

export class SignupComponent implements OnInit {
  loginForm: FormGroup;
  error:string = 'none';
  constructor(private fb: FormBuilder) 
   {
    this.loginForm = this.createFormGroup(this.fb);
   }

ngOnInit() {
   // alert('Called');
  } 
  onSubmit()
  {
    
    
   
    //console.log(this.loginForm.value);

    if(this.loginForm.invalid)
    {
      this.error = 'error';
    }
    else
    {
      this.error = 'success';
      console.log(this.loginForm.value);
    }
    
  }

  

  createFormGroup(fb: FormBuilder) { 
   return fb.group(
     {
      username:['',[Validators.required,Validators.minLength(4)]], 
      password:['',[Validators.required,Validators.minLength(4)]]
     })
  }
     
 

 get diagnostic() { 
 
 return JSON.stringify(this.loginForm.value); 
 
 return "";
 }

 errorAnimateDone()
 {
   this.error = 'none';
 }
}
