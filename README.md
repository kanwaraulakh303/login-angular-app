# Login application angular

This is a login application built using Angular 8 . Ui is based on Angular material and flex

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/webpack-dev-server/`. The app will automatically reload if you change any of the source files.

There is also an embedded node.js application.Which provides login web service endpoint. Mongo db is used as database. 

## In order to run the entire application.

Run npm install at the project root.

Run ng build --prod --output-path server/dist/`

Navigate to the directory `server`

Run nodemon server.js

Navigate to http://localhost:3000 you will see a full fledged Angular 8 login application running on node.js server.



