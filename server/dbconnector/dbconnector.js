const mongoose = require('mongoose');

module.exports = {
    
    connect:function()
    {
        let isProduction = process.env.NODE_ENV === 'production';
        mongoose.connect(gConfig.database_server, {useNewUrlParser: true, useUnifiedTopology: true});
        
        if(!isProduction)
        {
            mongoose.set('debug', true);
        }
    }
}