const config = require('../config/config.js');
const User = require("../models/user");
const jwt = require('jsonwebtoken');

module.exports = {
    
    post_login : function(req, res) {
        User.findOne({
            username: req.body.username
          }, function(err, user) {
            if (err) throw err;
        
            if (!user) {
              res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
            } else {
              // check if password matches
              user.comparePassword(req.body.password, function (err, isMatch) {
                if (isMatch && !err) {
                  // if user is found and password is right create a jwt token
                  var token = jwt.sign(user.toJSON(), gConfig.auth_secret_key);
                  // return the information including token as JSON
                  res.json({success: true, token: 'JWT ' + token});
                } else {
                  res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
                }
              });
            }
          });
    },
 
    
    post_signup : function(req,res)
    {
        console.log(req.body);
        if (!req.body.username || !req.body.password) {
            res.json({success: false, msg: 'Please pass username and password.'});
          } else {
            var newUser = new User({
              username: req.body.username,
              password: req.body.password
            });
            // save the user
            newUser.save(function(err) {
              if (err) {
                return res.json({success: false, msg: 'Username already exists.'});
              }
              res.json({success: true, msg: 'Successful created new user.'});
            });
          }
    }
}