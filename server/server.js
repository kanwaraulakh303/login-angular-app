const express = require('express');
const expressControllers = require('express-controller'); 
const config = require('./config/config.js');
const app = express()
const dbconnector = require('./dbconnector/dbconnector');
const errorhandler = require('errorhandler');
const passport = require('passport');
const path = require('path');

const bodyParser = require('body-parser')

//Environment
process.env.NODE_ENV = 'development';
var isProduction = process.env.NODE_ENV === 'production';


//set up database connection
dbconnector.connect(); 

//logger
app.use(require('morgan')('dev'));

app.use(bodyParser.urlencoded({ extended: false }));
if (!isProduction) {
    app.use(errorhandler())
  }

//session handler
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));

//initialize passport
app.use(passport.initialize());

//set passport strategy
require('./config/passport')(passport); 

//static files
app.use(express.static(__dirname + '/dist/'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/dist/index.html'); 
});

//bind controllers
expressControllers
            .setDirectory( __dirname + '/controllers')
            .bind(app);

// development error handler

if (!isProduction) {
    app.use(function(err, req, res, next) {
      console.log(err.stack);
  
      res.status(err.status || 500);
  
      res.json({'errors': {
        message: err.message,
        error: err
      }});
    });
}
  
// production error handler

  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({'errors': {
      message: err.message,
      error: {}
    }});
  });





           

app.listen(gConfig.node_port, () => console.log(`Chatting app listening on port ${gConfig.node_port}!`)) 